package com.agiletestingalliance;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;

public class CPDOFExameTest extends Mockito {


    @Test
    public void testBigger() throws Exception {

        int k = new MinMax().bigger(10, 5);
        assertEquals("Add", k, 10);

    }

    @Test
    public void testBiggerElse() throws Exception {

        int k = new MinMax().bigger(10, 11);
        assertEquals("Add", k, 11);

    }

    @Test
    public void checkDuration() {
        String message = new Duration().dur();
        assertEquals("CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.", message);
    }

    @Test
    public void checkUseful() {
        String message = new Usefulness().desc();
        assertEquals("DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.", message);
    }

    @Test
    public void checkAboutCPDF() {
        String message = new AboutCPDOF().desc();
        assertEquals("CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings.", message);
    }
}
